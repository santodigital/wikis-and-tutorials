# Primeiros passos com Front-end

## Primeiro instale os seguintes componentes

Última versão LTS do Node JS (https://nodejs.org/en/)

Última versão LTS do Yarn (https://yarnpkg.com/)

Última versão do VSCODE (https://code.visualstudio.com/download)

## Instale os complementos

Após ter feito as instalções iniciasis, clone ou faça download desse projeto, entre na pasta frontend e execute os seguintes comandos:

Para quem usa Linux:
```shell
cat vs_code_extensions_list.txt | xargs -n 1 code --install-extension
```

Para quem usa Windows:
```shell
start install-extentions-windows.bat
```

## Configurando o VSCode

### setings.json

Vá em "file > preferences > Settings" ou "Ctrl + ,"

No canto superior direito ao lado das abas clique no incone de arquivo externo "Open Settings (JSON)" ou procure por "edit in settings.json" na UI.

Edite o JSON com esse conteúdo:

```json
{
    "window.zoomLevel": 0,
    "javascript.validate.enable": false,
    "eslint.autoFixOnSave": true,
    "files.exclude": {
        "build/": true,
    },
    "editor.tabSize": 2,
    "javascript.updateImportsOnFileMove.enabled": "always",
    "editor.formatOnPaste": true,
    "editor.formatOnSave": false,
    "editor.formatOnType": false,
    "eslint.validate": [
        {
            "language": "javascript",
            "autoFix": true
        },
        {
            "language": "javascriptreact",
            "autoFix": true
        },
        {
            "language": "vue",
            "autoFix": true
        },
        {
            "language": "html",
            "autoFix": true
        },
        {
            "language": "scss",
            "autoFix": true
        },
        {
            "language": "css",
            "autoFix": true
        }
    ]
}
```

### Keybord shortcuts

Para facilitar na edição, caso prefira adicionar um atalho de teclado para auto formatar e corrigir o arquivo.

Vá em "file > preferences > Keybord Shortcuts" ou "Ctrl + K ou Ctrl + S"

No canto superior direito ao lado das abas clique no incone de arquivo externo "Open Keybord Shortcuts (JSON)"

```json
[
    {
        "key": "shift+alt+f",
        "command": "eslint.executeAutofix",
        "when": "editorTextFocus && !editorReadonly"
    }
]
```