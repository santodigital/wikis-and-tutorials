code --install-extension christian-kohler.npm-intellisense ^
call code --install-extension christian-kohler.path-intellisense ^
call code --install-extension CoenraadS.bracket-pair-colorizer ^
call code --install-extension dariofuzinato.vue-peek ^
call code --install-extension DavidAnson.vscode-markdownlint ^
call code --install-extension dbaeumer.vscode-eslint ^
call code --install-extension eamodio.gitlens ^
call code --install-extension eg2.vscode-npm-script ^
call code --install-extension formulahendry.auto-close-tag ^
call code --install-extension formulahendry.auto-rename-tag ^
call code --install-extension HaaLeo.vscode-stylint ^
call code --install-extension HookyQR.beautify ^
call code --install-extension michelemelluso.code-beautifier ^
call code --install-extension ms-python.python ^
call code --install-extension octref.vetur ^
call code --install-extension robertoachar.vscode-essentials-snippets ^
call code --install-extension shinworks.jsdoc-view ^
call code --install-extension sysoev.language-stylus ^
call code --install-extension vector-of-bool.gitflow ^
call code --install-extension xabikos.JavaScriptSnippets